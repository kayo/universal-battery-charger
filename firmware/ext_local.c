#include <ch.h>
#include <hal.h>

#include "main.h"
#include "ext_local.h"

#ifdef EXT_PWR
static void extPowerCheckI(void){
  static bool_t ext_pwr_ = FALSE;
  
  bool_t ext_pwr = palReadPad(PORT(EXT_PWR), PAD(EXT_PWR));
  
  if(ext_pwr != ext_pwr_){
    if(ext_pwr){ /* USB on */
      notifyI(USB_ENABLE);
    }else{ /* USB off */
      notifyI(USB_DISABLE);
    }
    ext_pwr_ = ext_pwr;
  }
}
#else
#define extPowerCheckI()
#endif

static void ext_callback(EXTDriver *extp, expchannel_t channel){
  (void)extp;
  (void)channel;
  
#ifdef EXT_PWR
  if(channel == PAD(EXT_PWR)){
    chSysLockFromIsr();
    extPowerCheckI();
    chSysUnlockFromIsr();
  }
#endif
}

static const EXTConfig ext_cfg = {
  {
#define B(P) {EXT_CH_MODE_BOTH_EDGES | EXT_CH_MODE_AUTOSTART | GPIO(EXT_MODE, P), ext_callback},
#define _() {EXT_CH_MODE_DISABLED, NULL},
    EXT_MAP
#undef B
#undef _
  }
};

void extLocalInit(void){
}

void extStartup(void){
  chSysLock();
  extPowerCheckI();
  chSysUnlock();
  
  extStart(&EXTD1, &ext_cfg);
}

void extShutdown(void){
  extStop(&EXTD1);
}
