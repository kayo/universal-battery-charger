#ifndef __main_h__
#define __main_h__ "main.h"

typedef enum {
  USB_ENABLE,
  USB_DISABLE,

  USB_ONLINE,
} Message;

extern Mailbox mbox;

#define notifyI(msg) chMBPostI(&mbox, msg);

#endif//__main_h__
