#include <string.h>
#include <stdlib.h>

#include "ch.h"
#include "hal.h"

#include "shell.h"
#include "chprintf.h"

#include "shell_local.h"

#define LINE_FEED "\r\n"

shellCommandDefine(echo){
  shellStdio(chp);
  
  int i, last = argc - 1;
  
  for(i = 0; i < argc; i++){
    chprintf(chp, "%s%s", argv[i], i == last ? LINE_FEED : " ");
  }
  
  return 0;
}

shellCommandDefine(free){
  (void)argv;
  shellStdio(chp);
  
  if (argc != 0) {
    chprintf(chp, "Usage: free" LINE_FEED);
    return -1;
  }

  size_t n, size;
  n = chHeapStatus(NULL, &size);
  
  chprintf(chp,
           "core free memory (bytes)    heap fragments    heap free total (bytes)" LINE_FEED
           "%24lu    %14lu    %23lu" LINE_FEED, chCoreStatus(), n, size);
  
  return 0;
}

shellCommandDefine(ps){
  (void)argv;
  static const char *states[] = {THD_STATE_NAMES};
  
  shellStdio(chp);
  
  if(argc != 0){
    chprintf(chp, "Usage: ps" LINE_FEED);
    return -1;
  }
  
  chprintf(chp, "    addr    stack prio refs     state       time" LINE_FEED);
  
  Thread *tp;
  
  for(tp = chRegFirstThread(); tp; tp = chRegNextThread(tp)){
    chprintf(chp,
             "%.8lx %.8lx %4lu %4lu %9s"
#if CH_DBG_THREADS_PROFILING == TRUE
             " %10lu"
#endif
#if CH_USE_REGISTRY == TRUE
             " %s"
#endif
             LINE_FEED,
             (uint32_t)tp, (uint32_t)tp->p_ctx.r13,
             (uint32_t)tp->p_prio, (uint32_t)(tp->p_refs - 1),
             states[tp->p_state]
#if CH_DBG_THREADS_PROFILING == TRUE
             , (uint32_t)tp->p_time
#endif
#if CH_USE_REGISTRY == TRUE
             , tp->p_name
#endif
             );
  }

  return 0;
}

shellCommandDefine(kill){
  shellStdio(chp);
  
  if(argc != 1){
    chprintf(chp, "Usage: kill [thread_addr|thread_name]" LINE_FEED);
    return -1;
  }
  
  const char *name = argv[0];
  uint32_t addr = atoi(name);
  
  Thread *tp;
  
  for(tp = chRegFirstThread(); tp; tp = chRegNextThread(tp)){
    if(addr == (uint32_t)tp || (tp->p_name && !strcasecmp(name, tp->p_name))){
      chThdTerminate(tp);
      return 0;
    }
  }

  chprintf(chp, "Invalid thread name or address." LINE_FEED);
  
  return -1;
}

#if HAL_USE_RTC == TRUE
shellCommandDefine(hwclock){
  shellStdio(chp);
  
  if (argc > 1) {
    chprintf(chp, "Usage: rtc [unix seconds to set][.milliseconds to set]\r\n");
    return -1;
  }
  
  RTCTime ts;
  
  if (argc > 0) {
    ts.tv_sec = atoi(argv[0]);
    
    const char *msec = strchr(argv[0], '.');
    ts.tv_msec = msec ? atoi(msec + 1) : 0;
    
    rtcSetTime(&RTCD1, &ts);
  } else {
    rtcGetTime(&RTCD1, &ts);
  }
  
  chprintf(chp, "%d.%03d\r\n", ts.tv_sec, ts.tv_msec);
  return 0;
}
#else
shellCommandDummy(hwclock);
#endif
