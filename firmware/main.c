/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 *  @file main.c
 *  @brief State machine
 *  @addtogroup MAIN
 *  @{
 */

#include "ch.h"
#include "hal.h"

#include "chprintf.h"

#include "main.h"
#include "ext_local.h"
#include "usb_local.h"
#include "bat_local.h"

#include "shell.h"
#include "shell_local.h"

shellStandardCommands();
shellAdvancedCommands();

batShellCommands();

static const ShellCommand *shell_cmd[] = {
  shellStandardCommandsRef()
  shellAdvancedCommandsRef()
  
  batShellCommandsRef()
  
  NULL
};

static const ShellConfig shell_cfg = {
  "shell",
  "Universal Battery Charger (" CORE_VERSION ")",
  "ubc> ",
  shell_cmd,
  (BaseSequentialStream*)&SDU1
};

static WORKING_AREA(waShell, 1024);
static ShellObject shell;

#define MBOX_SIZE 8
static msg_t mbox_buf[MBOX_SIZE];
//Mailbox mbox;
MAILBOX_DECL(mbox, mbox_buf, MBOX_SIZE);

typedef enum {
  IDLE,
  HOST,
} State;

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();
  
  extLocalInit();
  usbLocalInit();
  
  extStartup();
  
  //chMBInit(&mbox, mbox_buf, MBOX_SIZE);
  shellInit(&shell, &shell_cfg);
  
  Thread *shelltp = NULL;
  State state = IDLE;
  msg_t msg;
  
  for(; ; ){
    chMBFetch(&mbox, &msg, TIME_INFINITE);
    
    switch((Message)msg){
    case USB_ENABLE:
      if(state != HOST){
        batStartup();
        usbStartup();
        state = HOST;
      }
      break;
    case USB_ONLINE:
      if(state == HOST){
        if(!shelltp){
          shelltp = shellCreateStatic(&shell, waShell, sizeof(waShell), NORMALPRIO);
        }
      }
      break;
    case USB_DISABLE:
      if(state == HOST){
        if(shelltp){
          chThdTerminate(shelltp);
          shelltp = NULL;
        }
        usbShutdown();
        batShutdown();
        state = IDLE;
      }
      break;
    }
  }
}

/**
 * @}
 */
