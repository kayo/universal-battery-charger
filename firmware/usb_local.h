#ifndef __usb_local_h__
#define __usb_local_h__ "usb_local.h"

#if USB_CDC_COUNT > 0
extern SerialUSBDriver SDU1;
#if USB_CDC_COUNT > 1
extern SerialUSBDriver SDU2;
#else
#define SDU2 SDU1
#endif
#if USB_CDC_COUNT > 2
extern SerialUSBDriver SDU3;
#else
#define SDU3 SDU1
#endif
#endif

#if USB_MSC_COUNT > 0
#include "usb_msc.h"
extern MassStorageUSBDriver MSU1;
#endif

void usbLocalInit(void);

void usbStartup(void);
void usbShutdown(void);

#endif//__usb_local_h__
