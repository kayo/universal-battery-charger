#ifndef __usb_cdc_def_h__
#define __usb_cdc_def_h__ "usb_cdc_def.h"

#if !defined(USB_CDC_INTR_PACKET_SIZE)
#define USB_CDC_INTR_PACKET_SIZE 0x08
#endif

#if !defined(USB_CDC_DATA_PACKET_SIZE)
#define USB_CDC_DATA_PACKET_SIZE 0x20 //0x40
#endif

  /* CDC Functional Descriptor (3+dataLength bytes) */
#define USB_CDC_FUNCTIONAL(bDescriptorSubtype, dataLength)              \
  USB_DESC_BYTE(3 + (dataLength)), /* bLength */                        \
  USB_DESC_BYTE(0x24),             /* bDescriptorType (CS_INTERFACE) */ \
  USB_DESC_BYTE(bDescriptorSubtype)/* bDescriptorSubtype */
  
  /* CDC Functional Descriptors (5+5+4+5=19 bytes) */
#define USB_CDC_DESCRIPTORS(ifIntr, ifData)                             \
  /* Header Functional Descriptor (CDC section 5.2.3). (5 bytes) */     \
  USB_CDC_FUNCTIONAL    (0x00, 2), /* Header Functional */              \
  USB_DESC_BCD          (0x0110),  /* bcdCDC */                         \
  /* Call Management Functional Descriptor. (5 bytes) */                \
  USB_CDC_FUNCTIONAL    (0x01, 2), /* Call Management Functional */     \
  USB_DESC_BYTE         (0x00),    /* bmCapabilities (D0+D1) */         \
  USB_DESC_BYTE         (ifData),  /* bDataInterface */                 \
  /* ACM Functional Descriptor. */                                      \
  USB_CDC_FUNCTIONAL    (0x02, 1), /* Abstract Control Management */    \
  USB_DESC_BYTE         (0x02),    /* bmCapabilities */                 \
  /* Union Functional Descriptor. */                                    \
  USB_CDC_FUNCTIONAL    (0x06, 2), /* Union Functional */               \
  USB_DESC_BYTE         (ifIntr),  /* bMasterInterface (Communication Class Interface) */ \
  USB_DESC_BYTE         (ifData)   /* bSlaveInterface0 (Data Class Interface) */

#define USB_CDC_INTERFACE(ifIntr, idIntr, ifData, idData, epDataReq, epDataAvail, epIntrReq) \
  /* Interface Assotiation. (8 bytes) */                                \
  USB_DESC_INTERFACE_ASSOCIATION(ifIntr, /* bFirstInterface */          \
                                 0x02, /* bInterfaceCount */            \
                                 0x02, /* bFunctionClass */             \
                                 0x02, /* bFunctionSubClass */          \
                                 0x01, /* bFunctionProcotol */          \
                                 2),   /* iInterface */                 \
    /* Interface Descriptor. (9 bytes) */                               \
    USB_DESC_INTERFACE(ifIntr,         /* bInterfaceNumber */           \
                       0x00,           /* bAlternateSetting */          \
                       0x01,           /* bNumEndpoints */              \
                       0x02,           /* bInterfaceClass (Communications Interface Class, CDC section 4.2) */ \
                       0x02,           /* bInterfaceSubClass (Abstract Control Model, CDC section 4.3) */ \
                       0x01,           /* bInterfaceProtocol (AT commands, CDC section 4.4) */ \
                       idIntr),        /* iInterface */                 \
    /* CDC Functional Descriptors */                                    \
    USB_CDC_DESCRIPTORS(ifIntr, ifData),                                \
    /* Endpoint 2 Descriptor. (7 bytes) */                              \
    USB_DESC_ENDPOINT((epIntrReq)|0x80,                                 \
                      0x03,             /* bmAttributes (Interrupt) */  \
                      USB_CDC_INTR_PACKET_SIZE, /* wMaxPacketSize */    \
                      0xff),            /* bInterval */                 \
    /* Interface Descriptor. */                                         \
    USB_DESC_INTERFACE(ifData,          /* bInterfaceNumber */          \
                       0x00,            /* bAlternateSetting */         \
                       0x02,            /* bNumEndpoints */             \
                       0x0A,            /* bInterfaceClass (Data Class Interface, CDC section 4.5) */ \
                       0x00,            /* bInterfaceSubClass (CDC section 4.6) */ \
                       0x00,            /* bInterfaceProtocol (CDC section 4.7) */ \
                       idData),         /* iInterface */                \
    /* Endpoint 1 Descriptor. */                                        \
    USB_DESC_ENDPOINT(epDataAvail,      /* bEndpointAddress */          \
                      0x02,             /* bmAttributes (Bulk) */       \
                      USB_CDC_DATA_PACKET_SIZE, /* wMaxPacketSize */    \
                      0x00),            /* bInterval */                 \
    /* Endpoint 1 Descriptor. */                                        \
    USB_DESC_ENDPOINT((epDataReq)|0x80, /* bEndpointAddress */          \
                      0x02,             /* bmAttributes (Bulk) */       \
                      USB_CDC_DATA_PACKET_SIZE, /* wMaxPacketSize */    \
                      0x00)             /* bInterval */

#define USB_CDC_INTERFACE_LENGTH(count) ((count)*(      \
  8/* Interface Association */+                         \
  9/* Interface Descriptor (Abstract Control Model) */+ \
  5/* CDC Header Functional Descriptor */+              \
  5/* CDC Call Management Functional Descriptor */+     \
  4/* CDC ACM Functional Descriptor */+                 \
  5/* CDC Union Functional Descriptor */+               \
  7/* Interrupt Endpoint */+                            \
  9/* Interface Descriptor (Data Class Interface) */+   \
  7/* Data Available Endpoint */+                       \
  7/* Data Request Endpoint */))

#define USB_CDC_ENDPOINTS(prefix)                                   \
  static USBInEndpointState prefix ## _ep_intr_state;               \
  static const USBEndpointConfig prefix ## _ep_intr_config = {      \
    USB_EP_MODE_TYPE_INTR,                                          \
    NULL,                                                           \
    sduInterruptTransmitted,                                        \
    NULL,                                                           \
    USB_CDC_INTR_PACKET_SIZE,                                       \
    0x0000,                                                         \
    &prefix ## _ep_intr_state,                                      \
    NULL,                                                           \
    1,                                                              \
    NULL                                                            \
  };                                                                \
  static USBInEndpointState prefix ## _ep_data_in_state;            \
  static USBOutEndpointState prefix ## _ep_data_out_state;          \
  static const USBEndpointConfig prefix ## _ep_data_config = {      \
    USB_EP_MODE_TYPE_BULK,                                          \
    NULL,                                                           \
    sduDataTransmitted,                                             \
    sduDataReceived,                                                \
    USB_CDC_DATA_PACKET_SIZE,                                       \
    USB_CDC_DATA_PACKET_SIZE,                                       \
    &prefix ## _ep_data_in_state,                                   \
    &prefix ## _ep_data_out_state,                                  \
    1,                                                              \
    NULL                                                            \
  };

#define USB_CDC_INTR_CFG(prefix) &prefix ## _ep_intr_config
#define USB_CDC_DATA_CFG(prefix) &prefix ## _ep_data_config

#endif//__usb_cdc_def_h__
