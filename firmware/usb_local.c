#include "ch.h"
#include "hal.h"

#include "main.h"
#include "usb_local.h"

//#if USB_CDC_COUNT > 0
#include "usb_cdc_def.h"
//#endif

#include "usb_hid_def.h"

/* Specific section */

#ifndef usb_lld_disconnect_bus
#  define usb_lld_disconnect_bus(...)
#endif

#ifndef usb_lld_connect_bus
#  define usb_lld_connect_bus(...)
#endif

/*
 * Endpoints to be used for USBD1.
 */
#if USB_CDC_COUNT > 0
#define USBD1_CDC1_DATA_REQUEST_EP           1
#define USBD1_CDC1_DATA_AVAILABLE_EP         1
#define USBD1_CDC1_INTERRUPT_REQUEST_EP      2
#endif

#if USB_CDC_COUNT > 1
#define USBD1_CDC2_DATA_REQUEST_EP           3
#define USBD1_CDC2_DATA_AVAILABLE_EP         3
#define USBD1_CDC2_INTERRUPT_REQUEST_EP      4
#endif

#if USB_CDC_COUNT > 2
#define USBD1_CDC3_DATA_REQUEST_EP           5
#define USBD1_CDC3_DATA_AVAILABLE_EP         5
#define USBD1_CDC3_INTERRUPT_REQUEST_EP      6
#endif

#if USB_HID_COUNT > 0
//#include "usb_hid.h"
#define USBD1_HID1_REQ_RES_EP                3
#endif

/*
 * USB Device Descriptor.
 */
static const uint8_t usb_device_descriptor_data[18] = {
  USB_DESC_DEVICE(0x0200,        /* bcdUSB (2.0).                    */
                  0xef,          /* bDeviceClass (Misc).             */
                  0x02,          /* bDeviceSubClass (Common).        */
                  0x01,          /* bDeviceProtocol (IAD).           */
                  0x40,          /* bMaxPacketSize.                  */
                  0x0483,        /* idVendor (ST).                   */
                  0x5740,        /* idProduct.                       */
                  0x0200,        /* bcdDevice.                       */
                  1,             /* iManufacturer.                   */
                  2,             /* iProduct.                        */
                  3,             /* iSerialNumber.                   */
                  1)             /* bNumConfigurations.              */
};

/*
 * Device Descriptor wrapper.
 */
static const USBDescriptor usb_device_descriptor = {
  sizeof usb_device_descriptor_data,
  usb_device_descriptor_data
};

/* Configuration Descriptor tree for a CDC.*/
static const uint8_t usb_configuration_descriptor_data[9/* Configuration Descriptor */+
                                                       USB_CDC_INTERFACE_LENGTH(USB_CDC_COUNT)+
                                                       USB_HID_INTERFACE_LENGTH(USB_HID_COUNT)+USB_DESC_HID_LENGTH(1)+45+15] = {
  /* Configuration Descriptor. (9 bytes) */
  USB_DESC_CONFIGURATION(sizeof usb_configuration_descriptor_data,            /* wTotalLength */
                         USB_CDC_COUNT*2 + USB_HID_COUNT,         /* bNumInterfaces */
                         0x01,          /* bConfigurationValue */
                         0,             /* iConfiguration */
                         0xC0,          /* bmAttributes (self powered) */
                         50),           /* bMaxPower (100mA) */
#if USB_CDC_COUNT > 0
  /* CDC1 */
  USB_CDC_INTERFACE(0, 2, 1, 2,
                    USBD1_CDC1_DATA_REQUEST_EP,
                    USBD1_CDC1_DATA_AVAILABLE_EP,
                    USBD1_CDC1_INTERRUPT_REQUEST_EP),
#endif
#if USB_CDC_COUNT > 1
  /* CDC2 */
  USB_CDC_INTERFACE(2, 2, 3, 2,
                    USBD1_CDC2_DATA_REQUEST_EP,
                    USBD1_CDC2_DATA_AVAILABLE_EP,
                    USBD1_CDC2_INTERRUPT_REQUEST_EP),
#endif
#if USB_CDC_COUNT > 2
  /* CDC3 */
  USB_CDC_INTERFACE(4, 2, 5, 2,
                    USBD1_CDC3_DATA_REQUEST_EP,
                    USBD1_CDC3_DATA_AVAILABLE_EP,
                    USBD1_CDC3_INTERRUPT_REQUEST_EP),
#endif
#if USB_HID_COUNT > 0
  /* HID1 */
  USB_HID_INTERFACE(0+USB_CDC_COUNT*2, 5, 0, 0,
                    USBD1_HID1_REQ_RES_EP,
                    USBD1_HID1_REQ_RES_EP,
                    0x08, 0x0a),
  USB_DESC_HID(USB_BCD_HID,          /* bcdHID */
               0x00,                 /* bCountryCode */
               1),                   /* bNumDescriptor */
  USB_DESC_HID_ADD(0x22,             /* bDescriptorType */
                   45),              /* wDescriptorLenght */
  0x05, 0x85,         /*  Usage Page (Power Batsys),          */
  0xA0,               /*  Collection (Physical),              */
  0x09, 0x24,         /*      Usage (24h),                    */
  0xA1, 0x02,         /*      Collection (Logical),           */
  0x09, 0x44,         /*          Usage (44h),                */
  0x09, 0x45,         /*          Usage (45h),                */
  0x09, 0x46,         /*          Usage (46h),                */
  0x09, 0x47,         /*          Usage (47h),                */
  0x14,               /*          Logical Minimum (0),        */
  0x25, 0x01,         /*          Logical Maximum (1),        */
  0x75, 0x01,         /*          Report Size (1),            */
  0x95, 0x04,         /*          Report Count (4),           */
  0x81, 0x03,         /*          Input (Constant, Variable), */
  0x08,               /*          Usage (00h),                */
  0x75, 0x04,         /*          Report Size (4),            */
  0x95, 0x01,         /*          Report Count (1),           */
  0x81, 0x03,         /*          Input (Constant, Variable), */
  0x09, 0x65,         /*          Usage (65h),                */
  0x75, 0x08,         /*          Report Size (8),            */
  0x95, 0x01,         /*          Report Count (1),           */
  0x14,               /*          Logical Minimum (0),        */
  0x26, 0xFF, 0x00,   /*          Logical Maximum (255),      */
  0x81, 0x03,         /*          Input (Constant, Variable), */
  0xC0,               /*      End Collection,                 */
  0xC0                /*  End Collection                      */
#endif
};

/*
 * Configuration Descriptor wrapper.
 */
static const USBDescriptor usb_configuration_descriptor = {
  sizeof usb_configuration_descriptor_data,
  usb_configuration_descriptor_data
};

/*
 * U.S. English language identifier.
 */
static const uint8_t usb_string0[] = {
  USB_DESC_BYTE(4),                     /* bLength.                         */
  USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
  USB_DESC_WORD(0x0409)                 /* wLANGID (U.S. English).          */
};

/*
 * Vendor string.
 */
static const uint8_t usb_string1[] = {
  USB_DESC_BYTE(38),                    /* bLength.                         */
  USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
  'S', 0, 'T', 0, 'M', 0, 'i', 0, 'c', 0, 'r', 0, 'o', 0, 'e', 0,
  'l', 0, 'e', 0, 'c', 0, 't', 0, 'r', 0, 'o', 0, 'n', 0, 'i', 0,
  'c', 0, 's', 0
};

/*
 * Device Description string.
 */
static const uint8_t usb_string2[] = {
  USB_DESC_BYTE(56),                    /* bLength.                         */
  USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
  'C', 0, 'h', 0, 'i', 0, 'b', 0, 'i', 0, 'O', 0, 'S', 0, '/', 0,
  'R', 0, 'T', 0, ' ', 0, 'V', 0, 'i', 0, 'r', 0, 't', 0, 'u', 0,
  'a', 0, 'l', 0, ' ', 0, 'C', 0, 'O', 0, 'M', 0, ' ', 0, 'P', 0,
  'o', 0, 'r', 0, 't', 0
};

/*
 * Serial Number string.
 */
static const uint8_t usb_string3[] = {
  USB_DESC_BYTE(8),                     /* bLength.                         */
  USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
  '0' + CH_KERNEL_MAJOR, 0,
  '0' + CH_KERNEL_MINOR, 0,
  '0' + CH_KERNEL_PATCH, 0
};

#if USB_HID_COUNT > 0
/*
 * HID Interface string.
 */
static const uint8_t usb_string5[] = {
  USB_DESC_BYTE(70),                    /* bLength.                             */
  USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                     */
  'C', 0, 'h', 0, 'i', 0, 'b', 0, 'i', 0, 'O', 0, 'S', 0, '/', 0,
  'R', 0, 'T', 0, ' ', 0, 'V', 0, 'i', 0, 'r', 0, 't', 0, 'u', 0,
  'a', 0, 'l', 0, ' ', 0, 'B', 0, 'a', 0, 't', 0, 't', 0, 'e', 0,
  'r', 0, 'y', 0, ' ', 0, 'C', 0, 'h', 0, 'a', 0, 'r', 0, 'g', 0,
  'e', 0, 'r', 0
};
#endif

/*
 * Strings wrappers array.
 */
static const USBDescriptor usb_strings[] = {
  {sizeof usb_string0, usb_string0},
  {sizeof usb_string1, usb_string1},
  {sizeof usb_string2, usb_string2},
  {sizeof usb_string3, usb_string3},
#if USB_HID_COUNT > 0
  {sizeof usb_string5, usb_string5},
#endif
};

#define lengthof(array) (sizeof(array)/sizeof(array[0]))

/*
 * Handles the GET_DESCRIPTOR callback. All required descriptors must be
 * handled here.
 */
static const USBDescriptor *get_descriptor(USBDriver *usbp,
                                           uint8_t dtype,
                                           uint8_t dindex,
                                           uint16_t lang) {

  (void)usbp;
  (void)lang;
  switch (dtype) {
  case USB_DESCRIPTOR_DEVICE:
    return &usb_device_descriptor;
  case USB_DESCRIPTOR_CONFIGURATION:
    return &usb_configuration_descriptor;
  case USB_DESCRIPTOR_STRING:
    if (dindex < lengthof(usb_strings))
      return &usb_strings[dindex];
  }
  return NULL;
}

/*
 * Serial over USB Drivers and Endpoints.
 */

#if USB_CDC_COUNT > 0
SerialUSBDriver SDU1;
USB_CDC_ENDPOINTS(cdc1);
#endif

#if USB_CDC_COUNT > 1
SerialUSBDriver SDU2;
USB_CDC_ENDPOINTS(cdc2);
#endif

#if USB_CDC_COUNT > 2
SerialUSBDriver SDU3;
USB_CDC_ENDPOINTS(cdc3);
#endif

/*
 * Handles the USB driver global events.
 */
static void usb_event(USBDriver *usbp, usbevent_t event) {
  switch (event) {
  case USB_EVENT_RESET:
    return;
  case USB_EVENT_ADDRESS:
    return;
  case USB_EVENT_CONFIGURED:
    chSysLockFromIsr();
#if USB_CDC_COUNT > 0
    /* Enables the endpoints specified into the configuration.
       Note, this callback is invoked from an ISR so I-Class functions
       must be used.*/
    usbInitEndpointI(usbp, USBD1_CDC1_DATA_REQUEST_EP, USB_CDC_DATA_CFG(cdc1));
    usbInitEndpointI(usbp, USBD1_CDC1_INTERRUPT_REQUEST_EP, USB_CDC_INTR_CFG(cdc1));

    /* Resetting the state of the CDC subsystem.*/
    sduConfigureHookI(&SDU1);
#endif
#if USB_CDC_COUNT > 1
    /* Enables the endpoints specified into the configuration.
       Note, this callback is invoked from an ISR so I-Class functions
       must be used.*/
    usbInitEndpointI(usbp, USBD1_CDC2_DATA_REQUEST_EP, USB_CDC_DATA_CFG(cdc2));
    usbInitEndpointI(usbp, USBD1_CDC2_INTERRUPT_REQUEST_EP, USB_CDC_INTR_CFG(cdc2));

    /* Resetting the state of the CDC subsystem.*/
    sduConfigureHookI(&SDU2);
#endif
#if USB_CDC_COUNT > 2
    /* Enables the endpoints specified into the configuration.
       Note, this callback is invoked from an ISR so I-Class functions
       must be used.*/
    usbInitEndpointI(usbp, USBD1_CDC3_DATA_REQUEST_EP, USB_CDC_DATA_CFG(cdc3));
    usbInitEndpointI(usbp, USBD1_CDC3_INTERRUPT_REQUEST_EP, USB_CDC_INTR_CFG(cdc3));

    /* Resetting the state of the CDC subsystem.*/
    sduConfigureHookI(&SDU3);
#endif
    
    notifyI(USB_ONLINE);
    chSysUnlockFromIsr();
    return;
  case USB_EVENT_SUSPEND:
    return;
  case USB_EVENT_WAKEUP:
    return;
  case USB_EVENT_STALLED:
    return;
  }
  return;
}

static bool_t requests_hook(USBDriver *usbp) {
#if USB_CDC_COUNT > 0
  if(sduRequestsHook(usbp)){
    return TRUE;
  }
#endif
  return FALSE;
}

/*
 * USB driver configuration.
 */
static const USBConfig usb_cfg = {
  usb_event,
  get_descriptor,
  requests_hook,
  NULL
};

/*
 * Serial over USB driver configuration.
 */
#if USB_CDC_COUNT > 0
static const SerialUSBConfig cdc1_cfg = {
  &USBD1,
  USBD1_CDC1_DATA_REQUEST_EP,
  USBD1_CDC1_DATA_AVAILABLE_EP,
  USBD1_CDC1_INTERRUPT_REQUEST_EP
};
#endif
#if USB_CDC_COUNT > 1
static const SerialUSBConfig cdc2_cfg = {
  &USBD1,
  USBD1_CDC2_DATA_REQUEST_EP,
  USBD1_CDC2_DATA_AVAILABLE_EP,
  USBD1_CDC2_INTERRUPT_REQUEST_EP
};
#endif
#if USB_CDC_COUNT > 2
static const SerialUSBConfig cdc3_cfg = {
  &USBD1,
  USBD1_CDC3_DATA_REQUEST_EP,
  USBD1_CDC3_DATA_AVAILABLE_EP,
  USBD1_CDC3_INTERRUPT_REQUEST_EP
};
#endif

void usbLocalInit(void){
#if USB_CDC_COUNT > 0
  sduObjectInit(&SDU1);
#endif
#if USB_CDC_COUNT > 1
  sduObjectInit(&SDU2);
#endif
#if USB_CDC_COUNT > 2
  sduObjectInit(&SDU3);
#endif
}

void usbStartup(void){
  /*
   * Initializes a serial-over-USB CDC drivers.
   */
#if USB_CDC_COUNT > 0
  sduStart(&SDU1, &cdc1_cfg);
#endif
#if USB_CDC_COUNT > 1
  sduStart(&SDU2, &cdc2_cfg);
#endif
#if USB_CDC_COUNT > 2
  sduStart(&SDU3, &cdc3_cfg);
#endif
  
  /*
   * Activates the USB driver and then the USB bus pull-up on D+.
   * Note, a delay is inserted in order to not have to disconnect the cable
   * after a reset.
   */
  usbDisconnectBus(&USBD1);
  chThdSleepMilliseconds(1500);
  usbStart(&USBD1, &usb_cfg);
  usbConnectBus(&USBD1);
}

void usbShutdown(void){
  usbDisconnectBus(&USBD1);
  usbStop(&USBD1);

#if USB_CDC_COUNT > 0
  sduStop(&SDU1);
#endif
#if USB_CDC_COUNT > 1
  sduStop(&SDU2);
#endif
#if USB_CDC_COUNT > 2
  sduStop(&SDU3);
#endif
}
