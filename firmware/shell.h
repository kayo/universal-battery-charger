/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    shell.h
 * @brief   Simple CLI shell header.
 *
 * @addtogroup SHELL
 * @{
 */

#ifndef _SHELL_H_
#define _SHELL_H_

/**
 * @brief   Shell maximum input line length.
 */
#if !defined(SHELL_MAX_LINE_LENGTH) || defined(__DOXYGEN__)
#define SHELL_MAX_LINE_LENGTH       64
#endif

/**
 * @brief   Shell maximum arguments per command.
 */
#if !defined(SHELL_MAX_ARGUMENTS) || defined(__DOXYGEN__)
#define SHELL_MAX_ARGUMENTS         8
#endif

typedef struct ShellConfig  ShellConfig;
typedef struct ShellCommand ShellCommand;
typedef struct ShellObject  ShellObject;
typedef struct ShellLocal   ShellLocal;

/**
 * @brief   Command handler function type.
 */
typedef int (ShellProgram)(int argc, char *argv[]);

/**
 * @brief   Custom command entry type.
 */
struct ShellCommand {
  const char   *sc_name;      /**< @brief Command name.       */
  ShellProgram *sc_function;  /**< @brief Command function.   */
};

#define shellCommandExtern(name)                  \
  extern const ShellCommand shell_command_##name; \

#define shellCommandDefine(name)               \
  int shell_command_##name##_program           \
  (int argc, char *argv[]);                    \
  const ShellCommand shell_command_##name = {  \
    #name,                                     \
    shell_command_##name##_program,            \
  };                                           \
  int shell_command_##name##_program           \
  (int argc, char *argv[])

#define shellCommandDummy(name)                     \
  shellCommandDefine(name){                         \
    shellStdio(chp);                                \
    (void)argc;                                     \
    (void)argv;                                     \
    chprintf(chp, "Command not implemented.\r\n");  \
    return -1;                                      \
  }

/**
 * @brief   Shell descriptor type.
 */
struct ShellConfig {
  const char            *sc_name;
  const char            *sc_header;
  const char            *sc_prompt;
  const ShellCommand    **sc_commands;      /**< @brief Shell extra commands
                                               table.                     */
  BaseSequentialStream  *sc_channel;        /**< @brief I/O channel associated
                                               to the shell.              */
};

struct ShellObject {
  const ShellConfig     *so_config;
  
  EventSource           so_terminated;      /**< @brief Shell termination
                                                 event source.              */
};

struct ShellLocal {
  ShellObject *sl_shell;                    /**< @brief ShellObject pointer */
  const ShellCommand *sl_command;           /**< @brief Current ShellCommand pointer */
  char sl_line[SHELL_MAX_LINE_LENGTH];      /**< @brief Current ShellCommand line buffer */
  int sl_argc;                              /**< @brief Current ShellCommand argc */
  char *sl_argv[SHELL_MAX_ARGUMENTS + 1];   /**< @brief Current ShellCommand argv */
};

#define shellLocal() ((ShellLocal*)chThdLS())
#define shellSelf() (shellLocal()->sl_shell)
#define shellCommand() (shellLocal()->sl_command)
#define shellChannel() (shellSelf()->so_config->sc_channel)
#define shellStdio(var) BaseSequentialStream *var = shellChannel()

#define shellLinePtr() (shellLocal()->sl_line)
#define shellLineLen() sizeof(shellLinePtr())
#define shellLineEnd() (shellLinePtr() + shellLineLen() - 1)

#ifdef __cplusplus
extern "C" {
#endif
  void shellInit(ShellObject *sop, const ShellConfig *cfg);
  void shellExit(ShellObject *sop, msg_t msg);
  Thread *shellCreate(ShellObject *sop, size_t size, tprio_t prio);
  Thread *shellCreateStatic(ShellObject *sop, void *wsp,
                            size_t size, tprio_t prio);
  bool_t shellReadLine(ShellObject *sop);
  msg_t shellExecLine(ShellObject *sop);
  const ShellCommand *shellFindCommand(ShellObject *sop, const char *name);
#ifdef __cplusplus
}
#endif

#define shellCommandRef(name) &shell_command_##name,

#define shellStandardCommands()    \
  shellCommandExtern(help);        \
  shellCommandExtern(exit);        \
  shellCommandExtern(info);        \
  shellCommandExtern(systime)

#define shellStandardCommandsRef() \
  shellCommandRef(help)            \
  shellCommandRef(exit)            \
  shellCommandRef(info)            \
  shellCommandRef(systime)

#endif /* _SHELL_H_ */

/** @} */
