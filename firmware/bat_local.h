#ifndef __bat_local_h__
#define __bat_local_h__ "bat_local.h"

typedef enum {
  BAT_DISCHARGE,
  BAT_CHARGE,
} BATCheck;

void batStartup(void);
void batShutdown(void);

void batCheck(BATCheck check);

#define batShellCommands()    \
  shellCommandExtern(battery)

#define batShellCommandsRef() \
  shellCommandRef(battery)

#endif//__bat_local_h__
