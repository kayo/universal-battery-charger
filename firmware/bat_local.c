#include <ch.h>
#include <hal.h>

#include "main.h"
#include "shell.h"
#include "chprintf.h"

#include "bat_local.h"

static BATCheck bat_check = BAT_DISCHARGE;
static uint8_t bat_level = 0;

#define BAT_SAMPLES 8
static adcsample_t bat_samples[BAT_SAMPLES];

static void bat_callback(ADCDriver *adcp, adcsample_t *buffer, size_t n) {
  (void)adcp;
  
  if(n == BAT_SAMPLES){
    chSysLockFromIsr();
    /*
    uint64_t avg = 0;
    uint8_t i;
    
    for(i = 0; i < BAT_SAMPLES; i++){
      avg += bat_samples[i];
    }

    avg /= BAT_SAMPLES;
    bat_level = (avg - BAT_CHG_LOW) * 255 / (BAT_CHG_HIGH - BAT_CHG_LOW);
    
    if(bat_check == BAT_DISCHARGE){
      if(avg <= BAT_CHG_LOW){
        notifyI(BAT_LOW);
      }
    }else{
      if(avg >= BAT_CHG_HIGH){
        notifyI(BAT_HIGH);
      }
    }
    */
    chSysUnlockFromIsr();
  }
}

static const ADCConversionGroup bat_grpcfg = {
  FALSE,
  1,
  bat_callback,
  NULL,
  0, 0,                         /* CR1, CR2 */
  ADC_SMPR1_SMP_AN15(ADC_SAMPLE_55P5),
  0,                            /* SMPR2 */
  ADC_SQR1_NUM_CH(1),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN15),
};

void batStartup(void){
  adcStart(&ADCD1, NULL);
}

void batShutdown(void){
  adcStop(&ADCD1);
}

void batCheck(BATCheck check){
  bat_check = check;
  adcStartConversion(&ADCD1, &bat_grpcfg, bat_samples, BAT_SAMPLES);
}

#define LINE_FEED "\r\n"

shellCommandDefine(battery){
  shellStdio(chp);
  
  (void)argv;
  
  if(argc != 0){
    chprintf(chp,
             "Usage: battery" LINE_FEED
             "  Get battery status." LINE_FEED);
    return -1;
  }
  
  batCheck(BAT_CHARGE);
  chThdSleepMilliseconds(500);
  chprintf(chp, "%d%%" LINE_FEED, bat_level*100/256);
  
  return 0;
}
