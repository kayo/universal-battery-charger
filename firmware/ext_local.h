#ifndef __ext_local_h__
#define __ext_local_h__ "ext_local.h"

void extLocalInit(void);
void extStartup(void);
void extShutdown(void);

#endif//__ext_local_h__
