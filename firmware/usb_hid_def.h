#ifndef __usb_hid_def_h__
#define __usb_hid_def_h__ "usb_hid_def.h"

#define USB_DESCRIPTOR_HID 0x01
#define USB_BCD_HID 0x0101

#define USB_DESC_HID_ADD(bDescriptorType, wDescriptorLength)  \
  USB_DESC_BYTE(bDescriptorType),                             \
  USB_DESC_WORD(wDescriptorLength)
  
#define USB_DESC_HID_LENGTH(bNumDescriptors) (6+(bNumDescriptors)*2)

#define USB_DESC_HID(bcdHID, bCountryCode, bNumDescriptors)         \
  USB_DESC_BYTE(USB_DESC_HID_LENGTH(bNumDescriptors)), /* bLength */\
  USB_DESC_BYTE(0x21),            /* bDescriptorType */             \
  USB_DESC_BCD(bcdHID),           /* bcdHID */                      \
  USB_DESC_BYTE(bCountryCode),    /* bCountryCode */                \
  USB_DESC_BYTE(bNumDescriptors)  /* bNumDescriptors */

#define USB_HID_DATA_PACKET_SIZE 0x64 /* 100 bytes */
#define USB_HID_POLLING_INTERVAL 0x32 /* every 50ms */

#define USB_HID_INTERFACE(ifNum, ifStr, ifSubC, ifProt, epIn, epOut, pkS, iVal) \
  /* Interface Descriptor.*/                                         \
  USB_DESC_INTERFACE(ifNum,         /* bInterfaceNumber */           \
                     0x00,          /* bAlternateSetting */          \
                     0x02,          /* bNumEndpoints */              \
                     0x03,          /* bInterfaceClass (Human Device \
                                       Interface) */                 \
                     ifSubC,        /* bInterfaceSubClass (DCDHID    \
                                       page 8) */                    \
                     ifProt,        /* bInterfaceProtocol (DCDHID    \
                                       page 9) */                    \
                                                                     \
                     ifStr),        /* iInterface */                 \
  /* Endpoint 1 Descriptor */                                        \
  USB_DESC_ENDPOINT(epIn|0x80,      /* bEndpointAddress */           \
                    USB_EP_MODE_TYPE_INTR,    /* bmAttributes (Interrupt) */   \
                    pkS, /* wMaxPacketSize */                        \
                    iVal),/* bInterval */                            \
  /* Endpoint 2 Descriptor */                                        \
  USB_DESC_ENDPOINT(epOut,           /* bEndpointAddress */          \
                    USB_EP_MODE_TYPE_INTR,    /* bmAttributes (Interrupt) */  \
                    pkS,  /* wMaxPacketSize */   \
                    iVal) /* bInterval (ignored  \
                                                  for bulk) */

#define USB_HID_INTERFACE_LENGTH(count) ((count)*( \
  9/* Interface Descriptor (HID Device) */+        \
  7/* Data Input Endpoint */+                      \
  7/* Data Output Endpoint */))

#define HID_GENERIC_DESKTOP 0x01
#define HID_KEY_CODES 0x07
#define HID_LEDS 0x08
#define HID_BUTTONS 0x09
#define HID_POWER_DEVICE 0x84

#define HID_USAGE_PAGE(page)                    \
  USB_DESC_BYTE(0x05),                          \
  USB_DESC_BYTE(page)

#define HID_POINTER 0x01
#define HID_MOUSE 0x02
#define HID_KEYBOARD 0x06
#define HID_X 0x30
#define HID_Y 0x31
#define HID_FLOW 0x
#define HID_BATTERY_SYSTEM 0x85

#define HID_USAGE(type)                         \
  USB_DESC_BYTE(0x09),                          \
  USB_DESC_BYTE(type)

#define HID_PHYSICAL 0x00
#define HID_APPLICATION 0x01

#define HID_COLLECTION(type, ...)               \
  USB_DESC_BYTE(0xa1),                          \
  USB_DESC_BYTE(type),                          \
  __VA_ARGS__,                                  \
  USB_DESC_BYTE(0xc0)

#define HID_USAGE_MINIMUM(val)                  \
  USB_DESC_BYTE(0x19),                          \
  USB_DESC_BYTE(val)

#define HID_USAGE_MAXIMUM(val)                  \
  USB_DESC_BYTE(0x29),                          \
  USB_DESC_BYTE(val)

#define HID_LOGICAL_MINIMUM(val)                \
  USB_DESC_BYTE(0x15),                          \
  USB_DESC_BYTE(val)

#define HID_LOGICAL_MAXIMUM(val)                \
  USB_DESC_BYTE(0x25),                          \
  USB_DESC_BYTE(val)

#define HID_REPORT_SIZE(size)                   \
  USB_DESC_BYTE(0x75),                          \
  USB_DESC_BYTE(size)

#define HID_REPORT_COUNT(count)                 \
  USB_DESC_BYTE(0x95),                          \
  USB_DESC_BYTE(size)

#define HID_CONSTANT           (1 << 0)
#define HID_DATA               (0 << 0)
#define HID_VARIABLE           (1 << 1)
#define HID_ARRAY              (0 << 1)
#define HID_RELATIVE           (1 << 2)
#define HID_ABSOLUTE           (0 << 2)
#define HID_WRAP               (1 << 3)
#define HID_NO_WRAP            (0 << 3)
#define HID_NON_LINEAR         (1 << 4)
#define HID_LINEAR             (0 << 4)
#define HID_NO_PREFERRED_STATE (1 << 5)
#define HID_PREFERRED_STATE    (0 << 5)
#define HID_NULL_STATE         (1 << 6)
#define HID_NO_NULL_POSITION   (0 << 6)
#define HID_VOLATILE           (1 << 7)
#define HID_NON_VOLATILE       (0 << 7)
#define HID_BUFFERED_BYTES     (1 << 8)
#define HID_BITFIELD           (0 << 8)

#define HID_INPUT(type)                         \
  USB_DESC_BYTE(0x81),                          \
  USB_DESC_BYTE(type)

#define HID_OUTPUT(type)                        \
  USB_DESC_BYTE(0x91),                          \
  USB_DESC_BYTE(type)

#define HID_NONE 0x0
#define HID_VOLT 0x00f0d121
#define HID_CAMP 0x00100001
#define HID_HERTZ 0xf001
#define HID_WATT 0xd121
#define HID_KELVIN 0x00010001
#define HID_AMPSEC 0x00101001

#define HID_UNIT(unit)                          \
  USB_DESC_BYTE(0x),                            \
  USB_DESC_BYTE(unit)

#endif//__usb_hid_def_h__
