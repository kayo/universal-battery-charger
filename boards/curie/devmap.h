/* USB device wires */
#define USB_DM PA11
#define USB_DP PA12
#define USB_DISC PB10

/* 7-segment, 4-digits display */
#define L7S 4
/* segments */
#define L7S_A PB0
#define L7S_B PB1
#define L7S_C PB13
#define L7S_D PB14
#define L7S_E PB12
#define L7S_F PB11
#define L7S_G PB10
#define L7S_P PB15
/* digits */
#define L7S_1 PB8
#define L7S_2 PB7
#define L7S_3 PB6
#define L7S_4 PB5

/* external power */
#define EXT_PWR PC9
#define EXT_MAP _()_()_()_()_()_()_()_()_()B(EXT_PWR)_()_()_()_()_()_()

/* battery drivers */
#define BAT_PWM PWMD1
#define BAT_SEU ADCD1 /* sense voltage */
#define BAT_SEI ADCD2 /* sense current */

/* battery 1 charger */
#define BAT1_SEU PA0
#define BAT1_SEI PA1
#define BAT1_PWM 1
#define BAT1_OUT PA8

/* battery 2 charger */
#define BAT2_SEU PA2
#define BAT2_SEI PA3
#define BAT2_PWM 2
#define BAT2_OUT PA9
