/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

#define _CAT2(a, b) a##b
#define CAT2(a, b) _CAT2(a, b)
#define _CAT3(a, b, c) a##b##c
#define CAT3(a, b, c) _CAT3(a, b, c)
#define _CAT4(a, b, c, d) a##b##c##d
#define CAT4(a, b, c, d) _CAT4(a, b, c, d)

/* Not connected */
#define PNC_PORT NULL
#define PNC_PAD 0

#define PORT(x) CAT2(x, _PORT)
#define PAD(x) CAT2(x, _PAD)
#define GPIO(p, x) CAT2(p, CAT2(x, _GPIO))

#define ALTERNATE_PUSHPULL STM32_ALTERNATE_PUSHPULL
#define ALTERNATE_OPENDRAIN STM32_ALTERNATE_OPENDRAIN

/* Pad Modes:
 * RESET
 * UNCONNECTED
 * INPUT
 * INPUT_PUSHPULL
 * INPUT_PULLDOWN
 * INPUT_ANALOG
 * OUTPUT_PUSHPULL
 * OUTPUT_OPENDRAIN
 * ALTERNATE_PUSHPULL
 * ALTERNATE_OPENDRAIN
 */
#define configPad(x, m) palSetPadMode(PORT(x), PAD(x), CAT2(PAL_MODE_, m))

#define readPad(x) palReadPad(PORT(x), PAD(x))

#define getPad(x) (palReadLatch(PORT(x)) & PAL_PORT_BIT(PAD(x)))
#define setPad(x) palSetPad(PORT(x), PAD(x))
#define clearPad(x) palClearPad(PORT(x), PAD(x))

#define spiIO(spi, st) if(st){                        \
    configPad(CAT2(spi, _SCLK), ALTERNATE_PUSHPULL);  \
    configPad(CAT2(spi, _MOSI), ALTERNATE_PUSHPULL);  \
    configPad(CAT2(spi, _MISO), INPUT_PULLUP);        \
  }else{                                              \
    configPad(CAT2(spi, _SCLK), RESET);               \
    configPad(CAT2(spi, _MOSI), RESET);               \
    configPad(CAT2(spi, _MISO), RESET);               \
  }

#define spiCS(spi, cs, st) if(st){                   \
    configPad(CAT3(spi, _CS_, cs), OUTPUT_PUSHPULL); \
    setPad(CAT3(spi, _CS_, cs));                     \
  }else{                                             \
    configPad(CAT3(spi, _CS_, cs), RESET);           \
    clearPad(CAT3(spi, _CS_, cs));                   \
  }

#define adcIn(adc, ch, st) if(st){                \
    configPad(CAT3(adc, _CH_, ch), INPUT_ANALOG); \
  }else{                                          \
    configPad(CAT3(adc, _CH_, ch), RESET);        \
  }

#define ledSet(led, st) ((st) ? clearPad(led) : setPad(led))
#define ledTrig(led) ledSet(led, getPad(led))

#include "padmap.h"
#include "devmap.h"

/*
 * Board identifier.
 */
#define BOARD_CURIE
#define BOARD_NAME              "CURIE"

/*
 * Board frequencies.
 */
#define STM32_LSECLK            32768
#define STM32_HSECLK            8000000

/*
 * MCU type, supported types are defined in ./os/hal/platforms/hal_lld.h.
 */
#define STM32F10X_MD

/* PORT A */
#define VAL_GPIOACRL 0x88888888
#define VAL_GPIOACRH 0x88888888
#define VAL_GPIOAODR 0xFFFFFFFF

/* PORT B */
#define VAL_GPIOBCRL 0x88888888
#define VAL_GPIOBCRH 0x88888888
#define VAL_GPIOBODR 0xFFFFFFFF

/* PORT C */
#define VAL_GPIOCCRL 0x88888888
#define VAL_GPIOCCRH 0x88888888
#define VAL_GPIOCODR 0xFFFFFFFF

/* PORT D */
#define VAL_GPIODCRL 0x88888888
#define VAL_GPIODCRH 0x88888888
#define VAL_GPIODODR 0xFFFFFFFF

/* PORT E */
#define VAL_GPIOECRL 0x88888888
#define VAL_GPIOECRH 0x88888888
#define VAL_GPIOEODR 0xFFFFFFFF

/*
 * USB bus activation and de-activation macro, required by the USB driver.
 */
#ifdef USB_DISC
#define usb_lld_connect_bus(usbp) clearPad(USB_DISC)
#define usb_lld_disconnect_bus(usbp) setPad(USB_DISC)
#else
#define usb_lld_connect_bus(usbp)
#define usb_lld_disconnect_bus(usbp)
#endif

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
