/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"

/**
 * @brief   PAL setup.
 * @details Digital I/O ports static configuration as defined in @p board.h.
 *          This variable is used by the HAL when initializing the PAL driver.
 */
#if HAL_USE_PAL || defined(__DOXYGEN__)
const PALConfig pal_default_config =
{
  {VAL_GPIOAODR, VAL_GPIOACRL, VAL_GPIOACRH},
  {VAL_GPIOBODR, VAL_GPIOBCRL, VAL_GPIOBCRH},
  {VAL_GPIOCODR, VAL_GPIOCCRL, VAL_GPIOCCRH},
  {VAL_GPIODODR, VAL_GPIODCRL, VAL_GPIODCRH},
  {VAL_GPIOEODR, VAL_GPIOECRL, VAL_GPIOECRH}
};
#endif

/*
 * Early initialization code.
 * This initialization must be performed just after stack setup and before
 * any other initialization.
 */
void __early_init(void) {
  stm32_clock_init();
}

/*
 * Board-specific initialization code.
 */
void boardInit(void) {
#ifdef HAL_USE_USB
  configPad(USB_DM, INPUT);
  configPad(USB_DP, INPUT);
#endif

#ifdef USB_DISC
  configPad(USB_DISC, OUTPUT_PUSHPULL);
  setPad(USB_DISC);
#endif

#ifdef EXT_PWR
  configPad(EXT_PWR, INPUT_PULLDOWN);
#endif

#ifdef L7S
#define _(__)                                   \
  configPad(CAT2(L7S_,__), OUTPUT_PUSHPULL);    \
  clearPad(CAT2(L7S_,__));
  _(A)_(B)_(C)_(D)_(E)_(F)_(G)_(P);
#if L7S > 0
  _(1);
#endif
#if L7S > 1
  _(2);
#endif
#if L7S > 2
  _(3);
#endif
#if L7S > 3
  _(4);
#endif
#undef _
#endif
}
