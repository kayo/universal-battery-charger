;; Create pad map header
(defun mcu-helper-pad-map (from to)
  (interactive "cPad map from: \ncPad map from %s to: ")
  (mapc (lambda (pc)
          (let ((port (char-to-string (upcase pc))))
            (insert "/* PORT" port " */\n")
            (mapc (lambda (pn)
                    (let ((pad (number-to-string pn)))
                      (insert "#define P" port pad "_PORT GPIO" port "\n"
                              "#define P" port pad "_PAD " pad "\n"
                              "#define P" port pad "_GPIO _GPIO" port "\n")))
                  (number-sequence 0 15))
            (insert "\n")))
        (number-sequence from to)))
