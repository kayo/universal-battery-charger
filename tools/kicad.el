;; KiCAD Bill Of Materials helper (for new lisp-like format)
;; Usage:
;;  Add (require 'kicad) to .emacs
;;  Do M-x kicad-bom

(defun kicad-filter (data match &rest args)
  (delq nil (mapcar (lambda (item) (apply match item args)) data)))

(defun kicad-field (item field)
  (if (and (listp item) (eq field (car item))) (cdr item)))

(defun kicad-extract-field (item field &optional convert &rest args)
  (if (listp item)
      (car (mapcar (lambda (item) (apply convert item args))
                   (kicad-filter item 'kicad-field field)))))

(defun kicad-match-field (item field match &rest args)
  (if (and (listp item)
           (car (mapcar (lambda (item) (apply match item args))
                        (kicad-filter item 'kicad-field field))))
      item))

(defun kicad-extract-class (item)
  (replace-regexp-in-string "[[:digit:]]*$" "" (symbol-name (car item))))

(defun kicad-match-class (item class)
  (string= (kicad-extract-ref-class item) class))

(defun kicad-extract-value (item)
  (let ((value (car item)))
    (if (symbolp value) (symbol-name value)
      (if (numberp value) (number-to-string value) value))))

(defun kicad-match-value (actual needed)
  (string= (kicad-extract-value actual) needed))

(defun kicad-bom (netlist)
  "Loads KiCAD netlist in new format and creates Bill Of Materials."
  (interactive "fNetlist file: ")
  (let ((data)
        (components)
        (classes)
        (footprints)
        (values))
    (with-temp-buffer
      (insert "(setq data (cdr (quote )))")
      (goto-char (- (point-max) 3))
      (insert-file-contents netlist)
      (eval-buffer))
    (setq components (kicad-filter (car (kicad-filter data 'kicad-field 'components)) 'kicad-field 'comp))
    (setq classes (delete-dups (sort (kicad-filter components 'kicad-extract-field 'ref 'kicad-extract-class) 'string<)))
    (setq footprints (delete-dups (sort (kicad-filter components 'kicad-extract-field 'footprint 'kicad-extract-value) 'string<)))
    (setq values (delete-dups (sort (kicad-filter components 'kicad-extract-field 'value 'kicad-extract-value) 'string<)))
    ;;(setq values (kicad-filter components 'kicad-extract-field 'value 'kicad-extract-value))
    (find-file (replace-regexp-in-string "\.net$" ".bom" netlist))
    (erase-buffer)
    (mapc (lambda (class)
            (insert "\t#\t" class "\n-------------------------------\n")
            (let ((group-by-class (kicad-filter components 'kicad-match-field 'ref 'kicad-match-class class)))
              (mapc (lambda (footprint)
                      (let ((group-by-class-footprint (kicad-filter group-by-class 'kicad-match-field 'footprint 'kicad-match-value footprint)))
                        (mapc (lambda (value)
                                (let* ((group-by-class-footprint-value (kicad-filter group-by-class-footprint 'kicad-match-field 'value 'kicad-match-value value))
                                       (quantity (length group-by-class-footprint-value)))
                                  (if (> quantity 0)
                                      (insert "\t" (number-to-string quantity) "\t" footprint "\t" value "\n"))
                                  )) values)
                        )) footprints))
            (insert "\n")) classes)
    ))
